Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: plasma-pass
Source: https://invent.kde.org/plasma/plasma-pass

Files: *
Copyright: 2014, Sven Brauch <svenbrauch@gmail.com>
           2018, 2021, Daniel Vrátil <dvratil@kde.org>
License: LGPL-2+

Files: po/*
Copyright: 2018, 2020, Burkhard Lück <lueck@hube-lueck.de>
           2018, 2021, Stefan Asserhäll <stefan.asserhall@bredband.net>
           2018, Roman Paholik <wizzardsk@gmail.com>
           2018, Vit Pelcak <vit@pelcak.org>
           2019, 2021, Eloy Cuadra <ecuadra@eloihr.net>
           2019, 2021, Steve Allewell <steve.allewell@gmail.com>
           2019, 2021, Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
           2019, Adrián Chaves (Gallaecio) <adrian@chaves.io>
           2019, Alexander Potashev <aspotashev@gmail.com>
           2019, Jung Hee Lee <daemul72@gmail.com>
           2019, Karl Ove Hufthammer <karl@huftis.org>
           2019, Luiz Fernando Ranghetti <elchevive@opensuse.org>
           2019, Marek Laane <qiilaq69@gmail.com>
           2019, Paolo Zamponi <zapaolo@email.it>
           2019, Simon Depiets <sdepiets@gmail.com>
           2019, Stelios <sstavra@gmail.com>
           2019, Tommi Nieminen <translator@legisign.org>
           2019, pan93412 <pan93412@gmail.com>
           2019-2020, Shinjo Park <kde@peremen.name>
           2019-2021, Freek de Kruijf <freekdekruijf@kde.nl>
           2020, Alexander Yavorsky <kekcuha@gmail.com>
           2020, G Karunakar <karunakar@indlinux.org>
           2020, Iñigo Salvador Azurmendi <xalba@euskalnet.net>
           2020, Kheyyam Gojayev <xxmn77@gmail.com>
           2020, Kristóf Kiszel <kiszel.kristof@gmail.com>
           2020, Matjaž Jeran <matjaz.jeran@amis.net>
           2020, Sergiu Bivol <sergiu@cip.md>
           2020, enolp <enolp@softastur.org>
           2020, giovanni <g.sora@tiscali.it>
           2020, scootergrisen
           2021, A S Alam <aalam.yellow@gmail.com>
           2021, Xavier Besnard <xavier.besnard@neuf.fr>
License: LGPL-2+

Files: po/ca/plasma_applet_org.kde.plasma.pass.po
       po/ca@valencia/plasma_applet_org.kde.plasma.pass.po
       po/uk/plasma_applet_org.kde.plasma.pass.po
Copyright: 2018, Antoni Bella Pérez <antonibella5@yahoo.com>
           2019, 2021, Yuri Chornoivan <yurchor@ukr.net>
           2021, Josep Ma. Ferrer <txemaq@gmail.com>
License: LGPL-2.1-3-KDEeV

Files: debian/*
Copyright: 2021, Pino Toscano <pino@debian.org>
License: LGPL-2+

License: LGPL-2.1-3-KDEeV
 This file is distributed under the license LGPL version 2.1 or
 version 3 or later versions approved by the membership of KDE e.V.
 .
 On Debian systems, the complete texts of GNU Library General Public License
 version 2.1 and 3 can be found in "/usr/share/common-licenses/LGPL-2.1" and
 "/usr/share/common-licenses/LGPL-3".

License: LGPL-2+
 On Debian systems, the complete text of the GNU Library General Public License
 version 2 can be found in "/usr/share/common-licenses/LGPL-2".
